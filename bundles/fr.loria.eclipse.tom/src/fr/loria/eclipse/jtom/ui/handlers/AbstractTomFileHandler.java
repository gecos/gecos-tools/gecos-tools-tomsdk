package fr.loria.eclipse.jtom.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.handlers.HandlerUtil;

public abstract class AbstractTomFileHandler extends AbstractHandler {

	public abstract void handle(IFile file);

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
        ISelection selection = HandlerUtil.getCurrentSelection(event);
        if (selection instanceof StructuredSelection) {
        	StructuredSelection sselection = (StructuredSelection) selection;
			for (Object o : sselection.toArray()) {
				final IFile file = (IFile) (o);
				handle(file);
			}
        } else if (selection instanceof TextSelection) {
        	IEditorInput input = HandlerUtil.getActiveEditorInput(event);
        	final IFile file = (IFile)input.getAdapter(IFile.class);
        	handle(file);
        } else {
        	throw new UnsupportedOperationException("Unsupported selection : "+selection);
        }
        return null;
	}
	
	protected void refresh(final IResource r) {
		try {
			ResourcesPlugin.getWorkspace().run(new IWorkspaceRunnable() {
				@Override
				public void run(IProgressMonitor monitor) throws CoreException {
					r.getProject().refreshLocal(IResource.DEPTH_INFINITE, null);
				}
			}, null);
		} catch (CoreException e) { }
	}
	
}
