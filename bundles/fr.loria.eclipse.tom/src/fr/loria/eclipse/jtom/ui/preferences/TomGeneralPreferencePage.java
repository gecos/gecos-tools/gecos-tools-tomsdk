/*
 *   
 * TOM - To One Matching Compiler
 * 
 * Copyright (C) 2000-2004 Inria
 * Nancy, France.
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 * 
 * Pierre-Etienne Moreau  e-mail: Pierre-Etienne.Moreau@loria.fr
 * Julien Guyon						e-mail: Julien.guyon@loria.fr
 * 
 **/

package fr.loria.eclipse.jtom.ui.preferences;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PathEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

import fr.loria.eclipse.jtom.JtomPlugin;

/**
 * @author julien
 */
public class TomGeneralPreferencePage extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage {
	
	/**
	 * Constructor
	 */
	public TomGeneralPreferencePage() {
		super(FieldEditorPreferencePage.GRID);
		  // Set the preference store for the preference page.
		IPreferenceStore store =
			JtomPlugin.getDefault().getPreferenceStore();
		setPreferenceStore(store);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	protected void createFieldEditors() {
		


		addField(new SpacerFieldEditor(getFieldEditorParent()));
		addField(new LabelFieldEditor("Plug-in options:", getFieldEditorParent()));
		//display java errors
		BooleanFieldEditor displayJavaErrors = new BooleanFieldEditor(
				JtomPlugin.DISPLAY_JAVA_ERRORS_PREFERENCE, 
				"&Display Java Errors", 
				getFieldEditorParent());
		addField(displayJavaErrors);

		//verbose mode : the option doesn't really work
//		BooleanFieldEditor verboseMode = new BooleanFieldEditor(
//				JtomPlugin.VERBOSE_MODE_PREFERENCE, 
//				"&Verbose Mode (require Eclipse restart to enable/disable all messages)", 
//				getFieldEditorParent());
//		addField(verboseMode);
		
		// extension should not be removed as it is overwritten in the plugin.xml file
//		StringFieldEditor fileExtension = new StringFieldEditor(
//				JtomPlugin.TOM_EXTENSION_PREFERENCE,
//				"&File extension",
//				getFieldEditorParent());
//		addField(fileExtension);
		
		addField(new SpacerFieldEditor(getFieldEditorParent()));
		Link link = new Link(getFieldEditorParent(), SWT.NONE);
	    String message = "Compiler options (see <a href=\"http://tom.loria.fr/wiki/index.php5/Tom-2.10:Using_Tom\">Online help</a>) :";
		link.setText(message);
	    link.setSize(400, 100);
		link.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					// Open default external browser
					PlatformUI.getWorkbench().getBrowserSupport().getExternalBrowser().openURL(new URL(e.text));
				} catch (Exception ex) {}
			}
		});

		//not used
//		StringFieldEditor defaultCommand = new StringFieldEditor(
//				JtomPlugin.TOM_DEFAULT_COMMAND_PREFERENCE,
//				"&Default command line",
//				getFieldEditorParent());
//		addField(defaultCommand);
		
		final PathEditor listInclude = new PathEditor(
				JtomPlugin.TOM_INCLUDE_PREFERENCE,
				"&Include file path location",
				"Choose",
				getFieldEditorParent());
		addField(listInclude);

		Button button = new Button(getFieldEditorParent(), SWT.NONE);
	    button.setText("Find all .tom files in current workspace");
		button.setToolTipText("Only looks in the workspace opened projects. Ignores path containing \""
				+ File.separator
				+ "bin"
				+ File.separator
				+ "\" or \""
				+ File.separator
				+ "fr.loria.eclipse.tom"
				+ File.separator
				+ "\". Also ignores paths already present.");
	    button.addSelectionListener(new SelectionAdapter() {
	      @Override
	      public void widgetSelected(SelectionEvent e) {
	    	IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
	    	List<String> list = new ArrayList<>();
	  		for(IProject project : projects) {
	  			if (!project.isOpen()) continue; 
		  		String osString = project.getLocation().toOSString();
				File file = new File(osString);
				findAllTomFileFolders(file,list);
	  		}
	  		org.eclipse.swt.widgets.List listControl = listInclude.getListControl(getFieldEditorParent());
	  		for (String str : list) {
	  			if (listControl.indexOf(str) == -1)
	  				listControl.add(str);
	  		}
	      }
	    });
		Button button2 = new Button(getFieldEditorParent(), SWT.NONE);
	    button2.setText("Clear List");
	    button2.addSelectionListener(new SelectionAdapter() {
	      @Override
	      public void widgetSelected(SelectionEvent e) {
	  		listInclude.loadDefault();
	      }
	    });
	    
		//pretty print code
		BooleanFieldEditor prettyprint = new BooleanFieldEditor(
				JtomPlugin.PRETTY_PRINT_PREFERENCE, 
				"-p : Pretty prints output code", 
				getFieldEditorParent());
		addField(prettyprint);
		
		//multithread compilation
		BooleanFieldEditor multithread = new BooleanFieldEditor(
				JtomPlugin.MULTI_THREAD_PREFERENCE, 
				"-mt : Sets the multithread mode", 
				getFieldEditorParent());
		addField(multithread);
		
		//optimize
		BooleanFieldEditor optimize = new BooleanFieldEditor(
				JtomPlugin.OPTIMIZE_PREFERENCE, 
				"-O : Optimizes output code (removes unused variables and performs some inlining)", 
				getFieldEditorParent());
		addField(optimize);
		
		//optimize 2
		BooleanFieldEditor optimize2 = new BooleanFieldEditor(
				JtomPlugin.OPTIMIZE2_PREFERENCE, 
				"-O2 : Further optimizes generated code (does not imply -O)", 
				getFieldEditorParent());
		addField(optimize2);

//		//newtyper
//		BooleanFieldEditor newtyper = new BooleanFieldEditor(
//				JtomPlugin.USE_NEW_TYPER_PREFERENCE, 
//				"-nt : Uses New TyperPlugin", 
//				getFieldEditorParent());
//		addField(newtyper);
		
//		//inline
//		BooleanFieldEditor inline = new BooleanFieldEditor(
//				JtomPlugin.INLINE_PREFERENCE, 
//				"--inline : Inlines mapping ", 
//				getFieldEditorParent());
//		addField(inline);
//		
//		//inlineplus
//		BooleanFieldEditor inlineplus = new BooleanFieldEditor(
//				JtomPlugin.INLINE_PLUS_PREFERENCE, 
//				"--inlineplus : Forces inlining, even if no $ is used ", 
//				getFieldEditorParent());
//		addField(inlineplus);
		 
	}
	

	public void findAllTomFileFolders(File file,List<String> list) {
		if(file!=null && file.isDirectory()) {
			boolean found=false;
			File[] children = file.listFiles();
			for (File child : children) {
				String absolutePath = child.getAbsolutePath();
				if(child.isDirectory() && !absolutePath.contains(".metadata") ) {
					findAllTomFileFolders(child, list);
				}
				if (child.isFile() && absolutePath.endsWith(".tom")) {
					found=true;
				}
			}
			if(found && !file.getAbsolutePath().contains("/fr.loria.eclipse.tom/") && 
					!file.getAbsolutePath().contains("/bin/") && !file.getAbsolutePath().equals("/"))
				list.add(file.getAbsolutePath());
	    }
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
		// TODO Auto-generated method stub
	}
	
} //class TomGeneralPreferencePage
