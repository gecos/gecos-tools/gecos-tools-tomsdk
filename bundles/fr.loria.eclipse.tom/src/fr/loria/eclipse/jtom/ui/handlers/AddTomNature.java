package fr.loria.eclipse.jtom.ui.handlers;

import java.util.Arrays;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.internal.events.BuildCommand;
import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

public class AddTomNature  extends AbstractHandler {

	public static final String TOMNATURE = "fr.loria.eclipse.tomnature";
	
	public static final String[] BUILDERS = new String[] {
		"fr.loria.eclipse.tom.GomBuilder",
		"fr.loria.eclipse.tom.TomBuilder",
		"fr.loria.eclipse.tom.TomBuildAnalyser"
	};
	
	
	public static final boolean VERBOSE = true;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
        ISelection selection = HandlerUtil.getCurrentSelection(event);
		final IProject project;

        if (selection instanceof IStructuredSelection) {
            IStructuredSelection structuredSelection = (IStructuredSelection) selection;
            Object firstElement = structuredSelection.getFirstElement();
            if (firstElement instanceof IProject) {
            	project = (IProject) firstElement;
            } else if (firstElement instanceof IJavaProject) {
            	IJavaProject jp = (IJavaProject) firstElement;
            	project = jp.getProject();
            } else {
            	throw new IllegalArgumentException("[GeCoS] ERROR. Unsupported first element of structured selection : "+firstElement.getClass().getSimpleName());
            }
        } else {
        	throw new IllegalArgumentException("[GeCoS] ERROR. Unsupported selection : "+selection.getClass().getSimpleName());
        }
        if (VERBOSE) System.out.println("Add Tom nature to "+project);
		try {
			final IWorkspace ws = ResourcesPlugin.getWorkspace();
			ws.run(new IWorkspaceRunnable() {
				@Override
				public void run(IProgressMonitor monitor) throws CoreException {
					IProjectDescription description = project.getDescription();
					
					String[] natures = description.getNatureIds();
					for (String nature : natures) {
						if (nature.equals(TOMNATURE)) {
							if (VERBOSE) System.out.println("Project "+project.getName()+" already has Tom nature.");
							return;
						}
					}
					String[] newNatures = new String[natures.length + 1];
					System.arraycopy(natures, 0, newNatures, 1, natures.length);
					newNatures[0] = TOMNATURE;
					description.setNatureIds(newNatures);
					project.setDescription(description, null);
					

					ICommand[] buildSpecs = description.getBuildSpec();
					List<String> builders = Arrays.asList(BUILDERS);
					for (ICommand buildSpec : buildSpecs) {
						String name = buildSpec.getBuilderName();
						if (builders.contains(name)) {
							if (VERBOSE) System.out.println(name+ " already present");
							builders.remove(name);
						}
					}
					ICommand[] newBuildSpecs = new ICommand[buildSpecs.length + builders.size()];
					System.arraycopy(buildSpecs, 0, newBuildSpecs, 0, buildSpecs.length);
					int i = 0;
					for (String builderName : builders) {
						if (VERBOSE) System.out.println("adding "+builderName);
						BuildCommand c = new BuildCommand();
						c.setBuilderName(builderName);
						newBuildSpecs[buildSpecs.length+i] = c;
						i++;
					}
					description.setBuildSpec(newBuildSpecs);
					project.setDescription(description, null);
					
					project.refreshLocal(IResource.DEPTH_INFINITE, null);
					if (VERBOSE) System.out.println("Added Tom nature to project "+project.getName());
				}
			}, null);
			
		} catch (CoreException e) {
			e.printStackTrace(System.err);
			System.err.println("Could not add C nature to project "+project.getName());
		}
		return null;
	}
}