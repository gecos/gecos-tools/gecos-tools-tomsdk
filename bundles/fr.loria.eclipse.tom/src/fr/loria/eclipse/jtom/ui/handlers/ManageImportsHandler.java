package fr.loria.eclipse.jtom.ui.handlers;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;

/**
 * Copy imports from the generated Java file to the Tom source file. 
 * 
 * @author amorvan
 *
 */
public class ManageImportsHandler extends AbstractTomFileHandler {
	
	public void handle(IFile tomFile) {
		System.out.println("process file : "+tomFile);
		try {
			IResource tomFileResource = tomFile;
			IProject project = tomFileResource.getProject();
			IJavaProject jProject = JavaCore.create(project);
			if (jProject.isOnClasspath(tomFileResource)) {
				// look for the PackageFragmentRoot
				IPackageFragmentRoot packageFragmentRoot = null;
				IResource lookedResource = tomFileResource.getParent();
				while (packageFragmentRoot == null) {
					packageFragmentRoot = jProject.findPackageFragmentRoot(lookedResource.getFullPath());
					lookedResource = lookedResource.getParent();
					if (!jProject.isOnClasspath(lookedResource)) break;
				}
				if (packageFragmentRoot == null)
					return;
				
				String[] prePostImport = prePostImport(tomFile);
				String preImport = prePostImport[0];
				String postImport = prePostImport[1];

				String pa = filterPackageName(tomFile);
				IPath destDirPath = jProject.getProject().getParent().getLocation().append(packageFragmentRoot.getPath());
				IPath append = destDirPath.append(pa.replace(".", File.separator)+File.separator+tomFile.getName().substring(0,tomFile.getName().length()-2)+".java");

				File file = append.toFile();
				if (!file.exists() || !file.isFile())
					throw new RuntimeException();
				String importSection = getImportFromJavaFile(file);
				
				String outputFileContent = preImport+""+importSection+""+postImport;
				
				setIfDiff(outputFileContent, tomFile);
			} else
				return;

		} catch (CoreException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Set the content of tomFile to outputFileContent iff the content of
	 * tomFile differs from outputFileContent.
	 */
	private void setIfDiff(String outputFileContent, IFile tomFile) throws CoreException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(tomFile.getContents()));
		StringBuffer sb = new StringBuffer();
		while (br.ready())
			sb.append(br.readLine()+(br.ready()?"\n":""));
		br.close();
		String originalContent = sb.toString();
		if (originalContent.compareTo(outputFileContent) != 0) {
			InputStream is = new ByteArrayInputStream(outputFileContent.getBytes());
			tomFile.setContents(is, true, true, null);
			tomFile.touch(null);
		}
	}

	/**
	 * Extract the parts of the code in the source tomFile which precede and
	 * follow the import section.
	 */
	private String[] prePostImport(IFile tomFile) throws CoreException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(tomFile.getContents()));
		StringBuffer sb = new StringBuffer();
		while (br.ready())
			sb.append(br.readLine()+(br.ready()?"\n":""));
		br.close();
		String originalContent = sb.toString();
		
		Pattern compile = Pattern.compile("(?:/\\*(?:[^*]|(?:\\*+[^*/]))*\\*+/)|(?://.*)");
		Matcher matcher = compile.matcher(originalContent);
		
		int prevEnd = -1;
		int first = -1;
		int last = -1;
		
		/**
		 * 0 : looking for first import
		 * 1 : looking for first semicolon after last import
		 * 2 : done
		 */
		int state = 0;
		
		while(matcher.find()){
			int start = matcher.start();
			if (start > 0) {
				//actual text : look for import section
				String actualCode = originalContent.substring(prevEnd+1,matcher.start());
				switch (state) {
				case 0:
					Pattern p = Pattern.compile("\\s(import)\\s");
					Matcher m = p.matcher(actualCode);
					if (m.find()) {
						first = prevEnd+m.start()+1;
						state = 1;
					}
					if (state == 0)
						break;

				case 1:
					Pattern p2 = Pattern.compile("\\s(import)\\s");
					Matcher m2 = p2.matcher(actualCode);
					while (m2.find()) {
						int indexOf = actualCode.indexOf(";", m2.end());
						last = prevEnd+1+indexOf;
					}
					break;
				default:
					break;
				}
			}
			prevEnd = matcher.end();
		}
		if (prevEnd != originalContent.length()-1) {
			//actual text : look for import section
			String actualCode = originalContent.substring(prevEnd+1,originalContent.length()-1);
			switch (state) {
			case 0:
				Pattern p = Pattern.compile("\\s(import)\\s");
				Matcher m = p.matcher(actualCode);
				if (m.find()) {
					first = prevEnd+m.start()+1;
					state = 1;
				}
				if (state == 0)
					break;

			case 1:
				Pattern p2 = Pattern.compile("\\s(import)\\s");
				Matcher m2 = p2.matcher(actualCode);
				while (m2.find()) {
					int indexOf = actualCode.indexOf(";", m2.end());
					last = prevEnd+1+indexOf;
				}
				break;
			default:
				break;
			}
		}
		state = 2;
		
		if (first == -1)
			return new String[] {"",originalContent};
		else
			if (last == -1)
				last = originalContent.length()-1;
		
		return new String[] {originalContent.substring(0, first),originalContent.substring(last+1)};
	}
	
	/**
	 * Extract the part of the code in the source javaFile corresponding to the
	 * imports
	 */
	private String getImportFromJavaFile(File javaFile) throws CoreException, IOException {
		BufferedReader br2 = new BufferedReader(new FileReader(javaFile));
		StringBuffer sb = new StringBuffer();
		while (br2.ready())
			sb.append(br2.readLine()+"\n");
		br2.close();
		String originalContent = sb.toString();
		
		int prevEnd = -1;
		int first = -1;
		int last = -1;
		
		/**
		 * 0 : looking for first import
		 * 1 : looking for first semicolon after last import
		 * 2 : done
		 */
		int state = 0;

		Pattern compile = Pattern.compile("(?:/\\*(?:[^*]|(?:\\*+[^*/]))*\\*+/)|(?://.*)");
		Matcher matcher = compile.matcher(originalContent);
		while(matcher.find()){
			int start = matcher.start();
			if (start > 0) {
				//actual text : look for import section
				String actualCode = originalContent.substring(prevEnd+1,matcher.start());
				switch (state) {
				case 0:
					Pattern p = Pattern.compile("\\s(import)\\s");
					Matcher m = p.matcher(actualCode);
					if (m.find()) {
						first = prevEnd+m.start()+1;
						state = 1;
					}
					if (state == 0)
						break;

				case 1:
					Pattern p2 = Pattern.compile("\\s(import)\\s");
					Matcher m2 = p2.matcher(actualCode);
					while (m2.find()) {
						int indexOf = actualCode.indexOf(";", m2.end());
						last = prevEnd+1+indexOf+1;
					}
					break;
				default:
					break;
				}
			}
			prevEnd = matcher.end();
		}
		if (prevEnd != originalContent.length()-1) {
			//actual text : look for import section
			String actualCode = originalContent.substring(prevEnd+1,originalContent.length()-1);
			switch (state) {
			case 0:
				Pattern p = Pattern.compile("\\s(import)\\s");
				Matcher m = p.matcher(actualCode);
				if (m.find()) {
					first = prevEnd+m.start()+1;
					state = 1;
				}
				if (state == 0)
					break;

			case 1:
				Pattern p2 = Pattern.compile("\\s(import)\\s");
				Matcher m2 = p2.matcher(actualCode);
				while (m2.find()) {
					int indexOf = actualCode.indexOf(";", m2.end());
					last = prevEnd+1+indexOf+1;
				}
				break;
			default:
				break;
			}
		}
		state = 2;
		
		if (first == -1)
			return "";
		else
			if (last == -1)
				last = originalContent.length()-1;
		
		return originalContent.substring(first,last);
	}
	
	/**
	 * Extract package from tomFile contents (java syntax)
	 */
	private static String filterPackageName(IFile tomFile) throws CoreException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(tomFile.getContents()));
		String pa = null;
		boolean stop = false;
		while (br.ready() && !stop) {
			String line = br.readLine();
			if (line.contains("package"))
				pa = line;
			else {
				if (pa != null)
					pa += "\n"+line;
			}
			stop = (pa != null) && line.contains(";");
		}
		br.close();
		
		//remove comments from input String
		String noComment = pa.replaceAll("(?:/\\*(?:[^*]|(?:\\*+[^*/]))*\\*+/)|(?://.*)","");
		
		//remove package keyword
		int i = noComment.indexOf("package");
		if (i < 0)
			throw new RuntimeException("Extracted lines should contain the 'package' keyword.");
		String noPkg = noComment.substring(0,i)+noComment.substring(i+"package".length());
		
		//Strip (removes space/tab/newline) + remove last semicolon
		String noSpace = noPkg.replace("\n", "").replace(" ", "").replace("\t", "").replace(";","");
		return noSpace;
	}
}
